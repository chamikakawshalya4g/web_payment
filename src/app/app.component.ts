import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  isEditable = false;
  isPaymentStatus=false;

  constructor(private _formBuilder: FormBuilder
    ) {
      this.firstFormGroup = this._formBuilder.group({
        firstName: ['', [Validators.required, Validators.minLength(2)]],
        lastName: ['', [Validators.required, Validators.minLength(2)]],
        email: ['', [Validators.required, Validators.email]],
        phone: ['', [Validators.required, Validators.pattern('[0-9]{10}')]],
        address: ['', Validators.required],
        city: ['', Validators.required],
        country: ['', Validators.required],

      });
      this.secondFormGroup = this._formBuilder.group({
        quantity: [1,
          [
            Validators.required,
            Validators.max(5),
            Validators.min(1)
          ]
        ],     
      });
      
    }

  ngOnInit() {

  }
  $_C(name: string){
    return this.firstFormGroup.get(name) 
    
  }
  $_I(name: string){
    return this.secondFormGroup.get(name) 
  }
}
